package com.zhu.estest.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.zhu.estest.entity.EsBlog;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EsBlogRepositoryTest {
	@Autowired
	private EsBlogRepository esBlogRepository;
	
	@Before
	public void initRepository() {
		//清除所有数据
		esBlogRepository.deleteAll();
		//存入数据
		esBlogRepository.save(new EsBlog("登鹳雀楼","王之涣的诗",
				"白日依山尽，黄河入海流。欲穷千里目，更上一层楼。"));
		esBlogRepository.save(new EsBlog("相思","王维的诗",
				"红豆生南国，春来发几枝。愿君多采撷，此物最相思。"));
		esBlogRepository.save(new EsBlog("静夜思","李白的诗",
				"床前明月光，疑是地上霜。举头望明月，低头思故乡。"));
	}
	
	@Test
	public void test() {
		Pageable  pageable = new PageRequest(0, 20);
		String title = "思";
		String summary = "相思";
		String content = "相思";
		Page<EsBlog> page = esBlogRepository.findDistinctEsBlogByTitleContainingOrSummaryContainingOrContentContaining(
				title, summary, content, pageable);
		System.out.println(page.getTotalElements());
		for(EsBlog blog : page.getContent()) {
			System.out.println(blog);
		}
	}

}
