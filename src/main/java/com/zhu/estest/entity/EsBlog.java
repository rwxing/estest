package com.zhu.estest.entity;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "blog",type = "blog")//elasticsearch��ע��
public class EsBlog implements Serializable {

	private static final long serialVersionUID = 4564729518133694581L;
    @Id
	private String id;
	private String title;
	private String summary;
	private String content;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EsBlog(  String title, String summary, String content) {
		this.title = title;
		this.summary = summary;
		this.content = content;
	}

	public EsBlog() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "EsBlog [id=" + id + ", title=" + title + ", summary=" + summary + ", content=" + content + "]";
	}

}
