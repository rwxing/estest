package com.zhu.estest.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import com.zhu.estest.entity.EsBlog;

public interface EsBlogRepository extends ElasticsearchRepository<EsBlog, String> {
	/**
	 * ��ҳ��ѯ����
	 * @param title
	 * @param summary
	 * @param content
	 * @param pageable
	 * @return
	 */
   Page<EsBlog> findDistinctEsBlogByTitleContainingOrSummaryContainingOrContentContaining(String title,String summary,String content,Pageable pageable);
}
